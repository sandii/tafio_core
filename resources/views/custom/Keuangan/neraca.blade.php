@php
$tahun=$_GET['tahun']??date('Y');
$rutin= App\Models\project::rutin()->whereYear('created_at',$tahun)->pluck('omzet','bulan');
$nonrutin= App\Models\bukubesar::nonrutin()->whereYear('created_at',$tahun)->pluck('omzet','bulan');
$penggajian= App\Models\penggajian::omzet()->whereYear('created_at',$tahun)->pluck('omzet','bulan');
$belanja= App\Models\belanja::omzet()->whereYear('created_at',$tahun)->pluck('omzet','bulan');
$tunjangan= App\Models\tunjangan::omzet()->whereYear('created_at',$tahun)->pluck('omzet','bulan');


$totalkas=0;
foreach(App\Models\akundetil::kas()->get() as $kas)
$totalkas+=$kas->saldo;

$totalKasbon=collect(App\Models\member::all())->reduce(function($c, $kasbon){return $c + $kasbon->kasbonTerakhir;},0);

$piutangdagang=App\Models\project::totalpiutang()->piutang;

@endphp

      @component('tafio::widgets.before_table')
                @endcomponent

                    <table class="table-striped baris" data-toggle="table"  data-mobile-responsive="true">
                      <thead>
                          <tr>
                            <th>akun
                            <th>
<div class=uang>debet</div>
                            <th>
<div class=uang>kredit</div>
                          </tr>
                                        </thead>
                      <tbody>

<tr><td> <h3>kas<td>{!!uang($totalkas)!!}<td></tr>
<tr><td> <h3>piutang<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;piutang dagang<td>{!!uang($piutangdagang)!!}<td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;kasbon pegawai<td>{!!uang($totalKasbon)!!}<td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;piutang lain2<td><td></tr>
<tr><td> <h3>hutang<td><td></tr>
<tr><td> <h3>modal<td><td></tr>
<tr><td> <h3>prive<td><td></tr>
<tr><td> <h3>pemasukan<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;rutin<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;lain2<td><td></tr>
<tr><td> <h3>pengeluaran<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;gaji pegawai<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;tunjangan pegawai<td><td></tr>
<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;belanja<td><td></tr>
<tr><td> <h3>saldo<td><td></tr>



                      </tbody>


                </table>
