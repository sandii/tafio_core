<?php
// dd($halaman);
// extract($var);
$proses_list= App\Models\Proses::all()->pluck('nama','id');
$project_flow_list= App\Models\ProjectFlow::all()->pluck('nama','id');
$jadwal_list  = App\Models\ProjectFlow::Jadwal()->get();
$link_komplain=cek_alamat("komplain.create");
$link_edit=cek_alamat("project.detail.edit");
$link_schedule=cek_alamat("schedule.edit");
$dataModel=$halaman['parent_dataModel'];
?>

        @include('custom.jasa.header')
        <div class="container-fluid r-aside">
          @include('custom.jasa.card_order')
          @include('custom.jasa.card_jadwal')
          <div class=row>
            <div class="col-md-5">
              @include('custom.jasa.card_pengiriman')
                @can('aksesku','kurir/show')
                  @include('custom.jasa.card_hasil_pengiriman')
                @endcan
            </div>
            <div class='col-md-7'>
              @include('tafio::widgets.chat',['table_name'=>'project','table_id'=>$dataModel->id])
            </div>
          </div>
        </div>
      </div>
