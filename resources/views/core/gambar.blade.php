<?php
$folder=$nama_model.'/'.substr($nama_file,0,2).'/'.substr($nama_file,2,2);
?>
@if(Storage::exists($folder.'/'.$nama_file))

                                <div class="row el-element-overlay" style='width:160px'>

                                  <div class="el-card-item  p-0">
                                    <div class="el-card-avatar el-overlay-1 m-0 "> <img src='{!!asset(Storage::url($folder.'/'.$nama_file))!!}' alt="user"  />
                                      <div class="el-overlay ">
                                        <ul class="el-info">

                                          <li><a class="btn default btn-outline btn-xs image-popup-vertical-fit" href="{!!url(Storage::url($folder.'/'.$nama_file))!!}"><i class="icon-magnifier"></i></a></li>

                                          <li>
                                            {!! form_open(['method' => 'POST', 'files' => true, 'url' => 'gambar/upload/'.$nama_model.'/'.$id.'/'.$nama_field]) !!}

                                             <input type="file" name="gambar" id="file{{$id}}" class="inputfile"   onchange="this.form.submit()"    />
                                             <label for="file{{$id}}" ><a class="btn default btn-xs btn-outline"><i class="icon-cloud-upload"></i></a></label>
                                             {!! Form::close() !!}

                                          </li>
                                          <li><a class="btn default btn-xs btn-outline" href="{{url('gambar/hapus/'.$nama_model.'/'.$id.'/'.$nama_field)}}"  onclick="return confirm('Anda yakin akan menghapus gambar ini?')"><i class="icon-trash"></i></a></li>

                                        </ul>
                                      </div>
                                    </div>
                                  </div>

                                </div>

                              @else
                                 {!! form_open(['method' => 'POST', 'files' => true, 'url' => 'gambar/upload/'.$nama_model.'/'.$id.'/'.$nama_field]) !!}


                                  <input type="file" name="gambar"{{$id}} id="file{{$id}}" class="inputfile"  onchange="this.form.submit()"   />
                                  <label for="file{{$id}}" class="btn btn-primary btn-sm default btn-rounded" ><i class="icon-cloud-upload"></i></label>

                                  {!! Form::close() !!}

                                @endif
