- ## Get Started
    - [overview](/{{route}}/{{version}}/overview)
    - [contoh penulisan](/{{route}}/{{version}}/contoh)

- ## Core

    - [module](/{{route}}/{{version}}/module)
    - [page](/{{route}}/{{version}}/page)
    - [sub_page](/{{route}}/{{version}}/sub_page)


- ## Tambahan

    - [route shortcut](/{{route}}/{{version}}/route)
    - [aturan update composer](/{{route}}/{{version}}/composer)
