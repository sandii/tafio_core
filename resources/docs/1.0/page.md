# app/Tafio
---
##halaman()
struktur: 
```
     return [
      'judul'=>'judul',
      'nama'=>'nama model',
      'module'=>'',
      'display'=>[],
      'search'=>[],
      'card_tabs'=>['index'=>['Judul'=>'link'],'show'=>[],],
      'have_show'=>'true'
      'scope'=>['scope1','scope2'],
      'orderBy'=>['namaField','asc/desc']
      'customIndex'=>'path.ke.custom.view',
      'customForm'=>'path.ke.custom.form',
    ];
```

#### module <larecipe-badge type="danger" rounded>Wajib</larecipe-badge>
pilih dari yang ada: Publik, Config, Sdm, Jasa, Umum, Marketing, Keuangan

#### judul
diisi jika judul yang ditampilkan berbeda dengan nama kelas

#### nama
nama model yg dipakai di halaman (jika nama file model berbeda dengan nama file halaman)
termmasuk untuk sub halaman (ex: kas_bukubesar)

####display
untuk menentukan kolom2 yang ditampilkan di halaman index, show, create
contoh: `,'display'=>['index'=>['namaLengkap','jenisKelamin','alamat','telp/hp',]]`

#### search
menampilkan fitur pencarian dengan nama kolom di array
jika `'display'=>['index'=>[]]` ada isinya, hanya bisa pakai field yang ditampilkan di index saja

untuk search range tanggal dibuat :

` 'search'=>[['field'=>'created_at']]`


####card_tabs
     `'card_tabs'=>['index'=>['$Judul'=>'$link'],'show'=>[],],`
menampilkan tab $link di halaman index, show, create, atau edit dengan $Judul

####have_show
pilihan apakah punya halaman show

##fields()
struktur `'$namaKolom' => ['type' => '$typeData', 'judul' => 'judul kolom', 'validate' => 'required', 'formatIndex' => ['$type'=>'$format',...], 'depend'=>['field1','field2',...],'tab'=>'namaTab',] `

#### $namaKolom
nama kolom di halaman, 'namaKolom' akan ditampilkan menjadi 'Nama Kolom', jika 'judul'nya kosong.

#### $typeData
tipe data kolom. pilihannnya:
1. text
2. number
3. date
4. email
5. gambar
6. select -> perlu ada tambahan parameter `'select_field'=>'nama kolom'`
7. radio -> perlu ada tambahan parameter `'options'=> ['tampilanPilihan1'=>'nilaiPilihan1', 'tampilanPilihan2'=>'nilaiPilihan2', ...]`
8. formatIndex -> lihat keterangan formatIndex
9. table -> lihat keterangan table

#### judul kolom
judul yang ditampilkan di halaman jika tidak sama dengan nama kolom

#### validate
validasi untuk field ini

#### formatIndex
pilihan 
1. link, contoh `"link"=>"member/{id}/cuti`
2. link_show, contoh `'link_show'=>'true'`
3. tanggal, $formatnya ikut format tanggal standar. contoh `'tanggal'=>'d-m-Y'`

#### tab
memulai tab baru untuk field ini dan field2 di bawahnya sampai ada field dengan tab lainnya lagi
contoh: tab tarbiyah dimulai dari jumlahBukuIslam sampai waktuKhususMembaca, kemudian ada tab baru foto 
```
    'jumlahBukuIslam'=>['type'=>'text','tab'=>'tarbiyah'],
           'tokohSahabatDikagumi'=>['type'=>'text'],
           'halaqahPalingDekat'=>['type'=>'text'],
           'waktuKhususMembaca'=>['type'=>'text'],
     'foto'=>['type'=>'gambar','tab'=>'foto']
```

#### table
membuat tabel terpisah. perlu ditambahi daftar kolom.
Contoh:
```
 'education'=>['type'=>'table','tab'=>'riwayatPendidikan','judul'=>'pendidikan formal',
           'column' =>['jenjangPendidikan'=>'jenjang',
                     'tahunLulus'=>'tahun lulus',
                     'institusi'=>'nama institusi pendidikan',
                     'jurusan'=>'jurusan',
                     'status'=>'lulus(L)/ Belum lulus(BL)/ tidak lulus (TL)'],
                   ],
```
