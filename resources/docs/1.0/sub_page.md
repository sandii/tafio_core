#app/Tafio/page_subpage.php

Untuk membuat halaman (child page) yang berkaitan dengan halaman di atasnya (parent page). Misalnya halaman daftar cuti dari seorang karyawan.

##Nama file
nama file terdiri dari 2 kata yang dipisahkan '_'.  kata pertama merupakan nama parent halaman, dan kata kedua merupakan nama anak halaman. 
contoh: ``halaman_subhalaman.php``

##Nama Kelas
harus sama dengan nama file : `'halaman_subhalaman'`

##nama model
jika nama model berbeda dengan nama halaman dan sub halaman, maka perlu ditulis di halaman() dengan `'nama'=>'parentModel_childModel'`
