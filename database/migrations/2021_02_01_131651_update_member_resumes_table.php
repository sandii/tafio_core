<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMemberResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public $nama_tabel="member_resumes";
    public function up()
    {

  Schema::table($this->nama_tabel, function (Blueprint $table) {
  $table->integer("pace")->nullable();
        });

   }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  Schema::table($this->nama_tabel, function (Blueprint $table)
  {

$table->dropColumn("pace");

});


}
}
