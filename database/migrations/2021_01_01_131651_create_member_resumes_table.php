<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public $nama_tabel="member_resumes";
    public function up()
    {
  Schema::create($this->nama_tabel, function (Blueprint $table) {
            $table->bigIncrements('id');
          $table->unsignedBigInteger('company_id')->nullable();
            $table->timestamps();

          $table->unsignedBigInteger('strava_id')->nullable();
            $table->integer('distance')->nullable();
            $table->integer('elapsed_time')->nullable();
            $table->integer('total_activities')->nullable();
            $table->string('type')->nullable();
            $table->integer('year')->nullable();
            $table->string('event')->nullable();
            $table->integer('event_id')->nullable();
                  });

      Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('company_id')->references('id')->on('companies')
            ->onUpdate('cascade')->onDelete('cascade');
            });
        Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('strava_id')->references('strava_id')->on('members')
            ->onUpdate('cascade')->onDelete('cascade');
            });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropForeign(['company_id']);
          $table->dropForeign(['strava_id']);
        });
        Schema::dropIfExists($this->nama_tabel);
    }
}
