<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

public $nama_tabel="members";
    public function up()
    {
  Schema::table($this->nama_tabel, function (Blueprint $table) {

            $table->integer('strava_id')->nullable();
            $table->string('strava_username')->nullable();
            $table->string('refresh_token')->nullable();
            $table->string('access_token')->nullable();
            $table->integer('expires_at')->nullable();
        });

   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropColumn('strava_id');
          $table->dropColumn('strava_username');
          $table->dropColumn('refresh_token');
          $table->dropColumn('access_token');
          $table->dropColumn('expires_at');
        });
    }
}
