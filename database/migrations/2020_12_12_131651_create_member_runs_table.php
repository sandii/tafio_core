<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public $nama_tabel="member_runs";
    public function up()
    {
  Schema::create($this->nama_tabel, function (Blueprint $table) {
            $table->bigIncrements('id');
          $table->unsignedBigInteger('company_id')->nullable();
            $table->timestamps();

          $table->unsignedBigInteger('member_id')->nullable();
            $table->timestamp('date')->nullable();
            $table->time('time')->nullable();
            $table->float('distance',3,1)->nullable();
            $table->float('pace',4,2)->nullable();
                  });

      Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('company_id')->references('id')->on('companies')
            ->onUpdate('cascade')->onDelete('cascade');
            });
      Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members')
            ->onUpdate('cascade')->onDelete('cascade');
            });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropForeign(['company_id']);
          $table->dropForeign(['member_id']);
        });
        Schema::dropIfExists($this->nama_tabel);
    }
}
