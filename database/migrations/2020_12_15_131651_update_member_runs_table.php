<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMemberRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public $nama_tabel="member_runs";
    public function up()
    {

  Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropColumn('time');
          $table->dropColumn('date');
          $table->dropColumn('distance');
        });

  Schema::table($this->nama_tabel, function (Blueprint $table) {
$table->string("name")->nullable();
  $table->integer("distance")->nullable();
$table->integer("moving_time")->nullable();
  $table->integer("elapsed_time")->nullable();
  $table->float("total_elevation_gain",8,2)->nullable();
  $table->string("type")->nullable();
  $table->timestamp("start_date")->nullable();
  $table->float("average_speed",4,2)->nullable();
  $table->float("max_speed",4,2)->nullable();
  $table->float("average_cadence",5,2)->nullable();
        });

   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  Schema::table($this->nama_tabel, function (Blueprint $table)
  {

$table->dropColumn("name");
  $table->dropColumn("distance");
$table->dropColumn("moving_time");
  $table->dropColumn("elapsed_time");
  $table->dropColumn("total_elevation_gain");
  $table->dropColumn("type");
  $table->dropColumn("start_date");
  $table->dropColumn("average_speed");
  $table->dropColumn("max_speed");
  $table->dropColumn("average_cadence");
});

  Schema::table($this->nama_tabel, function (Blueprint $table)
  {
$table->float("time",5,2)->nullable();
  $table->timestamp("date")->nullable();
$table->float("distance",8,2)->nullable();
});
}
}
