<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

public $nama_tabel="members";
    public function up()
    {
  Schema::create($this->nama_tabel, function (Blueprint $table) {
            $table->bigIncrements('id');
          $table->unsignedBigInteger('company_id')->nullable();

            $table->string('nama')->nullable();
            $table->string('tempatLahir')->nullable();
            $table->date('tglLahir')->nullable();
            $table->string('jenisKelamin')->nullable();
            $table->string('alamat')->nullable();
            $table->string('telp/hp')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('namaTempatKerja')->nullable();
            $table->string('alamatPekerjaan')->nullable();
            $table->string('foto')->nullable();
            $table->string('nim')->nullable();
            $table->string('kota')->nullable();
            $table->timestamps();
        });

      Schema::table($this->nama_tabel,function (Blueprint $table) {
            $table->foreign('company_id')->references('id')->on('companies')
            ->onUpdate('cascade')->onDelete('cascade');
            });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropForeign(['company_id']);
        });
        Schema::dropIfExists($this->nama_tabel,);
    }
}
