<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameMemberRunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public $nama_tabel="member_runs";
    public function up()
    {
  Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropForeign(['member_id']);
        });

  Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->renameColumn('member_id','strava_id');
        });

  Schema::table('members', function (Blueprint $table) {
          $table->unsignedBigInteger('strava_id')->change();
        });

  Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->unsignedBigInteger('strava_id')->change();
        });

          Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('strava_id')->references('strava_id')->on('members')
            ->onUpdate('cascade')->onDelete('cascade');
            });

   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  Schema::table($this->nama_tabel, function (Blueprint $table) {
          $table->dropForeign(['strava_id']);
          $table->renameColumn('strava_id','member_id');
        });
          Schema::table($this->nama_tabel, function (Blueprint $table) {
            $table->foreign('member_id')->references('id')->on('members')
            ->onUpdate('cascade')->onDelete('cascade');
            });

    }
}
