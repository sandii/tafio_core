<?php

namespace App\Tafio;
use Tafio\Library\Resource;



class member_user  extends Resource
{

  public function halaman()
  {

    $member=$this->ambil_parameter("member");
    return [
            'module'=>'Database',
            'judul'=>['anggota','user'],
            'contentView'=>'detail'
,'card_tabs'=>['index'=>['data '=>'member/'.$member,
                                    'akses'=>'active']]


        ];
  }
  public function fields()
  {
    return ['email'=>['type'  => 'email','validate'=>'email'],
            'password'=>['type'  => 'password'],
            'authlevel'=>['judul'=>'akses level','type'=>'select','select_field'=>'nama','default'=>1001,'disabled'=>true]

                                   ];


  }

  }
