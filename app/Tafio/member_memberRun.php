<?php

namespace App\Tafio;
use Tafio\Library\Resource;



class member_memberRun  extends Resource
{

  public function halaman()
  {

    $member=$this->ambil_parameter("member");
    return [
            'module'=>'Running'
        ];
  }
  public function fields()
  {
    return [
'start_date'=>['judul'=>'tanggal'],
'name'=>[],
'type'=>[],
'distance'=>[],
'moving_time'=>['judul'=>'moving <br>time'],
'pace'=>[],
'elapsed_time'=>['judul'=>'elapsed <br> time'],
'total_elevation_gain'=>['judul'=>'total <br>elevation'],
'speed'=>['judul'=>'average <br>speed'],
'max_speed'=>['judul'=>'max<br>speed'],
'strava'=>[],
   'strava'=>['formatIndex'=>[
            'text'=>'detail','link_new_window'=>'https://strava.com/activities/{id}']]
                                   ];


  }
  }
