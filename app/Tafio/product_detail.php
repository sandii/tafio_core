<?php

namespace App\Tafio;
use Tafio\Library\Resource;

class product_detail extends Resource
{
  
  public function halaman()
  {
    return [
        'module'=>'Database',
        'judul'=>['product','gambar']
    ];

  }

  public function fields()
  {

     
    return [
        'product'=>['judul'=>'Product'],
        'photos'=>[],
        'is_default'=>[]
    ];
  }

}
