<?php

namespace App\Tafio;
use Tafio\Library\Resource;

class status extends Resource
{
  public function halaman()
  {
    return [
      'judul'=>'Status',
      'module'=>'Database',
    ];

  }

  public function fields()
  {

    return [
      'name'=>['type'=>'text']
    ];

  }

}
