<?php

namespace App\Tafio;
use Tafio\Library\Resource;

class productDetail extends Resource
{
  public function halaman()
  {
    $product=$this->ambil_parameter("product");
    return [
      'judul'=>'Gallery',
      'module'=>'Database'
    ];

  }

  public function fields()
  {
    return [
      'product'=>['type'=>'select','select_field'=>'name','validate' => 'required'],
      'photos'=>['type'=>'gambar']
    ];
  }

}
