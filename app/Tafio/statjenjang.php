<?php

namespace App\Tafio;
use Tafio\Library\Resource;

use Auth;


class statjenjang  extends Resource
{

  public function halaman()
  {

    return [ 'nama'=>'member',
            'module'=>'Running',
            'judul'=>'statistik jenjang',
            'scope'=>['jenjang'],
            'contentView'=>'chart',
            'jenisChart'=>'ColumnChart' // isinya  ColumnChart/AreaChart

        ];
  }

  public function fields()
  {
    return ['dpc->nama'=>[],
    'muda'=>[],
    'madya'=>[],
    'dewasa'=>[],
    'ahli'=>[],
  ];
  }



  }
