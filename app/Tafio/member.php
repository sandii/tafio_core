<?php

namespace App\Tafio;
use Tafio\Library\Resource;

use Auth;
use App\Models\member as model_member;

class member  extends Resource
{

public function setVariable()
{
$this->member=$this->ambil_parameter("member");
}

  public function halaman()
  {
    return ['judul'=>'anggota',
            'module'=>'Database'
            ,'display'=>['index'=>['nim','nama','kota','telp/hp','strava_id','statistic']]
            ,'search'=>[['field'=>'nama'],]
            ,'orderBy'=>'nama'
            ,'card_tabs'=>['show'=>['data '=>'active',
                                    'akses'=>'member/'.$this->member.'/user']]
//default route index,edit,destroy
            ,'have_show'=>'true',
        ];

  }

  public function fields()
  {

    return [
      'nim'=>['type'=>'text','sortable'=>true],
      'nama'=>['type'  => 'text','sortable'=>true,'validate' => 'required','formatIndex'=>['link_show'=>'true']] ,
      'namaPanggilan'=>['type'=>'text'],
           'tempatLahir'=>['type'=>'text'],
            'tglLahir'=>['type'=>'date'],
           'jenisKelamin'=>['type'=>'radio','options'=>['laki-laki'=>'laki-laki','perempuan'=>'perempuan']],
           'alamat'=>['type'=>'text'],
           'kota'=>['type'=>'text','judul'=>'kota/kabupaten'],
           'telp/hp'=>['type'=>'text'],
           'statistic'=>['formatIndex'=>['text'=>'detail','link'=>'member/{id}/memberRun']],
           'pekerjaan'=>['type'=>'text'],
           'namaTempatKerja'=>['type'=>'text'],
           'alamatPekerjaan'=>['type'=>'text'],
           'strava_id'=>['judul'=>'strava','formatIndex'=>[
            'if_empty'=>'','link_new_window'=>'https://strava.com/athletes/{strava_id}']]
         ];
  }



  }
