<?php

namespace App\Tafio;
use Tafio\Library\Resource;

class category extends Resource
{
  public function halaman()
  {
    return [
      'judul'=>'product',
      'module'=>'Database',
    ];

  }

  public function fields()
  {

    return [
      'name'=>['type'=>'text']
    ];

  }

}
