<?php

namespace App\Tafio;
use Tafio\Library\Resource;

use Auth;


class Whattodo  extends Resource
{

  public function halaman()
  {

    return ['parent_dataModel'=>Auth::user(),
            'module'=>'Publik',
        ];
  }

  public function fields()
  {
    return ['isi'=>['type'  => 'textarea','validate' => 'required']];
  }
}
