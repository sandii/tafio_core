<?php

namespace App\Tafio;
use Tafio\Library\Resource;

use Auth;


class statanggota  extends Resource
{

  public function halaman()
  {

    return [ 'nama'=>'member',
            'module'=>'Running',
            'judul'=>'statistik jenis kelamin',
            'scope'=>['kelamin'],
            'contentView'=>'chart',
            'jenisChart'=>'ColumnChart' // isinya  ColumnChart/AreaChart

        ];
  }

  public function fields()
  {
    return ['dpc->nama'=>[],
    'laki'=>['judul'=>'laki-laki'],
    'perempuan'=>[],
  ];
  }



  }
