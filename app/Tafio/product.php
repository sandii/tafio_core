<?php

namespace App\Tafio;
use Tafio\Library\Resource;
use App\Models\product as model_product;

class product extends Resource
{
  
  public function setVariable()
  {
    $this->product=$this->ambil_parameter("product");
  }

  public function halaman()
  {
    return [
      'judul'=>'product',
      'module'=>'Database',
      'have_show'=>'true',
      'card_tabs'=>['show'=>['data '=>'active']]
    ];

  }

  public function fields()
  {
    return [
      'name'=>['type'=>'text','validate' => 'required','formatIndex'=>['link_show'=>'true']],
      'slug'=>['type'=>'text','validate' => 'required'],
      'category'=>['type'=>'select','select_field'=>'name','validate' => 'required'],
      'description'=>['type'=>'text'],
      'price'=>['type'=>'number','validate' => 'required'],
      'quantity'=>['type'=>'number','validate' => 'required'],
      'gambar'=>['formatIndex'=>['text'=>'gambar','link'=>'product/{id}/detail']],
    ];
  }

}
