<?php

namespace App\Tafio;
use Tafio\Library\Resource;
use Carbon\Carbon;

use Auth;

class weekly  extends Resource
{

  public function halaman()
  {

    return ['judul'=>'leaderboard > weekly',
            'module'=>'Database',
            'nama'=>'memberResume',
			'scope'=>['event'=>['Run','minggu']],
      'orderBy'=>['distance'=>'desc']
            ,'search'=>[['field'=>'year','type'=>'number','default'=>date('Y')],
            ['judul'=>'week','field'=>'event_id','type'=>'number','default'=>Carbon::now()->week()]]
,'card_tabs'=>['index'=>['weekly '=>'weekly',
                                    'monthly'=>'monthly',
                                    'yearly'=>'yearly',
                                  ]]
        ];

  }

  public function fields()
  {

    return [
      'member->nama'=>['formatIndex'=>[
            'link_new_window'=>'https://strava.com/athletes/{strava_id}']],
      'distance'=>['sortable'=>true],
      'elapsed_time'=>[],
      'total_activities'=>['sortable'=>true],
      'pace'=>['sortable'=>true],
               ];
  }



  }
