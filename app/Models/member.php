<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Kyslik\ColumnSortable\Sortable;
class member extends Model
{
	use sortable;
protected $fillable = [
            'nama',
            'company_id',

           'tempatLahir',
            'tglLahir',
           'jenisKelamin',
           'alamat',
           'telp/hp',
           'pekerjaan',
           'namaTempatKerja',
           'alamatPekerjaan',
           'nim',
           'foto',
           'kota',
           'refresh_token',
          'access_token',
          'strava_id',
          'strava_username',
          'expires_at'
    ];

    public function company(){
      return $this->belongsTo(company::class);
    }
   public function user()
    {
        return $this->hasOne("Tafio\Models\User");
      }
  public function memberRun(){
      return $this->hasMany(memberRun::class,'strava_id','strava_id');
    }

  public function memberResume(){
      return $this->hasMany(memberResume::class,'strava_id','strava_id');
    }


  }
