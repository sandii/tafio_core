<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Str;
use Kyslik\ColumnSortable\Sortable;

class memberResume extends Model
{
	use sortable;
protected $fillable = [
	'id',
'strava_id',
'distance',
'elapsed_time',
'total_activities',
'type',
'year',
'event',
'event_id',
'company_id',
'pace',
    ];


    public function company(){
      return $this->belongsTo(company::class);
    }

    public function member(){
      return $this->belongsTo(member::class,'strava_id','strava_id');
    }

	public function scopeEvent($query,$var){
       $query->where('type',$var[0]);
		 $query->where('event',$var[1]);
return $query;
	}

public function getDistanceAttribute($value)
    {
        return number_format($value/1000,2,'.','');
    }
public function getElapsedTimeAttribute($value)
    {
return floor($value/3600).":".date("i:s",$value);
    }

public function getPaceAttribute($value)
    {

return date("i:s",$value);

    }

  }
