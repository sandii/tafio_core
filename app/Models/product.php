<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class product extends Model
{
    use SoftDeletes;
    
    protected $table = 'products';
    protected $fillable = [
        'name',
        'slug',
        'category_id',
        'description',
        'price',
        'quantity',
        'company_id'
    ];

    public function company(){
        return $this->belongsTo(company::class);
    }

    public function detail()
    {
        return $this->hasMany(productDetail::class);
    }

    public function category(){
        return $this->belongsTo(category::class);
    }

    // public function getPriceAttribute($value)
    // {
    //     return number_format($value);
    // }

}
