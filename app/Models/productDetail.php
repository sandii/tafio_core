<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class productDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'photos',
        'is_default',
        'company_id'
    ];

    public function company()
    {
        return $this->belongsTo(company::class);
    }

    public function product()
    {
        return $this->belongsTo(product::class);
    }
    
    public function getPhotosAttribute($value)
    {
        return url('storage/',$value);
    }
}
