<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class status extends Model
{
    use SoftDeletes;
    protected $table = 'statuses';
    protected $fillable = [
        'name',
        'company_id'
    ];

    public function company(){
        return $this->belongsTo(company::class);
    }

    public function transaction(){
        return $this->hasMany(transaction::class);
    }
}
