<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
class memberRun extends Model
{
protected $fillable = [
	'id',
    'name',
'strava_id',
'distance',
'moving_time',
'pace',
'elapsed_time',
'total_elevation_gain',
'type',
'start_date',
'average_speed',
'max_speed',
'average_cadence',
            'company_id',
    ];

    public function company(){
      return $this->belongsTo(company::class);
    }

    public function member(){
      return $this->belongsTo(member::class,'strava_id','strava_id');
    }
public function getDistanceAttribute($value)
    {
        return number_format($value/1000,2);
    }
public function getMovingTimeAttribute($value)
    {
        return number_format($value/60,2);
    }
public function getElapsedTimeAttribute($value)
    {
        return number_format($value/60,2);
    }


public function getSpeedAttribute($value)
    {
        return number_format($this->average_speed/1000*3600,2);
    }
public function getMaxSpeedAttribute($value)
    {
        return $value/1000*3600;
    }

public function getNameAttribute($value)
    {
        return Str::limit($value,20);
    }

public function getPaceAttribute($value)
    {
		$total=0;
			if($this->average_speed!=0)
			$total=1000/$this->average_speed;

return date("i:s",$total);

    }
  }
