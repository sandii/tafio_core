<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'company_id'
    ];

    public function company(){
        return $this->belongsTo(company::class);
    }

    public function product(){
        return $this->hasMany(product::class);
    }
    
}
