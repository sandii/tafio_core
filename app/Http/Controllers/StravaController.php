<?php

namespace App\Http\Controllers;
use Auth;

use App\Events\PushStrava;
use Carbon\Carbon;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\member;
use App\Models\memberRun;
use App\Models\temp;

class StravaController extends Controller
{
    public function index()
    {
return redirect('https://www.strava.com/oauth/authorize?client_id=59093&redirect_uri=https://elektross.id/strava/proses&response_type=code&approval_prompt=auto&scope=read_all,activity:read_all&state=test');
    }


public function proses(Request $request)
{

$response = Http::post('https://www.strava.com/api/v3/oauth/token', [
  'client_id'=>59093,
  'client_secret'=>'4c2760b96e83ffa8dde2c98a98ca2156386a0230',
  'code'=>$request->input('code'),
  'grant_type'=>'authorization_code',
]);

$hasil=json_decode($response,true);

$member=Auth::user()->member;
// dd($member);
$member->refresh_token=$hasil['refresh_token'];
$member->access_token=$hasil['access_token'];
$member->expires_at=$hasil['expires_at'];
$member->strava_id=$hasil['athlete']['id'];
$member->strava_username=$hasil['athlete']['username'];
$member->save();

return redirect('personal');


}

public function subscribe()
{
$response = Http::post('https://www.strava.com/api/v3/push_subscriptions',[
'client_id'=>59093,
'client_secret'=>'4c2760b96e83ffa8dde2c98a98ca2156386a0230',
'callback_url'=>'https://elektross.id/strava/kiriman',
'verify_token'=>'STRAVA',
]);

 echo $response;

}
//// di route aktifkan dulu, defaultnya mati
public function hapus_subscribe()
{
$response = Http::delete('https://www.strava.com/api/v3/push_subscriptions/178269',[
'client_id'=>59093,
'client_secret'=>'4c2760b96e83ffa8dde2c98a98ca2156386a0230',
]);

 echo $response;

}


///// utk ngecek subscripion nya
public function cek()
{


$response = Http::get('https://www.strava.com/api/v3/push_subscriptions',[
'client_id'=>59093,
'client_secret'=>'4c2760b96e83ffa8dde2c98a98ca2156386a0230',
]);

 echo $response;


}


public function validation(Request $request)
{

return response()->json(['hub.challenge' => $request->hub_challenge],200);

$flight = Temp::create([
    'owner_id' => 1234,
    'updates' => $request->hub_challenge
]);


}

public function kiriman(Request $request)
{
 $owner=$request->input('owner_id');
 $jenis=$request->input('object_type');
 $operasi=$request->input('aspect_type');
 $run_id=$request->input('object_id');

 $flight = Temp::create([
     'owner_id' => $owner,
     'updates' => $request->getContent(),
 ]);
//
 if($jenis!='activity')
 {
 return response('OK', 200);
 exit();
}

$member=member::firstWhere('strava_id',$owner);

 if($operasi=='delete')
 {
$type=memberRun::find($run_id)->type;
$destroy=memberRun::destroy($run_id);


$this->hitung_resume($member,$type);

 return response('OK', 200);
 exit();
}
 else if($operasi=='create' or $operasi=='update')
 {

//sementara----------------------------------------
//$owner=47552523;
//$run_id=4589622681;
////////////////////////////////////////////////////

$this->cek_token($member);


$response = Http::withToken($member->access_token)
->get('https://www.strava.com/api/v3/activities/'.$run_id,[
'include_all_efforts'=>'',
]);

$hasil=json_decode($response,true);


$this->update_run($hasil,$member);


$this->hitung_resume($member,$hasil['type']);

}

 return response('OK', 200);


}


public function update_run($hasil,$member)
{
$type=$hasil['type'];
if($type=='VirtualRun')
$type='Run';
$flight = memberRun::updateOrCreate([
    'id' => $hasil['id'],
    'company_id'=>$member->company_id,
'strava_id'=>$hasil['athlete']['id']],[
    'name' => $hasil['name'],
    'distance' => $hasil['distance'],
    'moving_time' => $hasil['moving_time'],
    'elapsed_time' => $hasil['elapsed_time'],
    'total_elevation_gain' => $hasil['total_elevation_gain'],
    'type' => $type,
    'start_date' => $hasil['start_date_local'],
    'average_speed' => $hasil['average_speed'],
    'max_speed' => $hasil['max_speed'],
]);
}


public function hitung_resume($member,$type)
{

if($type=='VirtualRun')
$type='Run';

if($type!='Run' and $type!='Ride')
return false;

$sekarang=Carbon::now();
$query="sum(distance)as total_jarak,sum(elapsed_time)as total_waktu, count(*)as total";
$company=$member->company_id;
$bulan=date('n');
$tahun=date('Y');
$minggu=$sekarang->week();

//bulanan
$bulanan=$member->memberRun()->select(DB::raw($query))
->where('type',$type)->whereMonth('start_date','=',$bulan)->first();



$member->memberResume()->updateOrCreate(
['company_id'=>$company,'year'=>$tahun,'event'=>'bulan','event_id'=>$bulan,'type'=>$type],[
'distance'=>$bulanan->total_jarak,
'elapsed_time'=>$bulanan->total_waktu,
'total_activities'=>$bulanan->total,
'pace'=>($bulanan->total_waktu/$bulanan->total_jarak*1000??0),
]
);



$mingguan=$member->memberRun()->select(DB::raw($query))
->where('type',$type)->whereBetween('start_date',[Carbon::now()->startOfWeek(),(Carbon::now()->endOfWeek())])->first();

$member->memberResume()->updateOrCreate(
['company_id'=>$company,'year'=>$tahun,'event'=>'minggu','event_id'=>$minggu,'type'=>$type],[
'distance'=>$mingguan->total_jarak,
'elapsed_time'=>$mingguan->total_waktu,
'total_activities'=>$mingguan->total,
'pace'=>($mingguan->total_waktu/$mingguan->total_jarak*1000??0),
]
);

$tahunan=$member->memberRun()->select(DB::raw($query))
->where('type',$type)->whereYear('start_date','=',$tahun)->first();


$member->memberResume()->updateOrCreate(
['company_id'=>$company,'year'=>$tahun,'event'=>'tahun','type'=>$type],[
'distance'=>$tahunan->total_jarak,
'elapsed_time'=>$tahunan->total_waktu,
'total_activities'=>$tahunan->total,
'pace'=>($tahunan->total_waktu/$tahunan->total_jarak*1000??0),
]
);

}



public function cek_token($member)
{
if($member->expires_at < time())
{


///ambil access_token yg baru
$response = Http::post('https://www.strava.com/api/v3/oauth/token',[
'client_id'=>59093,
'client_secret'=>'4c2760b96e83ffa8dde2c98a98ca2156386a0230',
'grant_type'=>'refresh_token',
'refresh_token'=>$member->refresh_token,
]);
$hasil=json_decode($response,true);
$member->refresh_token=$hasil['refresh_token'];
$member->access_token=$hasil['access_token'];
$member->expires_at=$hasil['expires_at'];
$member->save();


}
}

public function coba()
{
// $this->grab_semua_data();

$member=member::find(17);
// $this->hitung_resume($member,"Run");
$this->grab_semua_data($member);
}

public function grab_semua_data($member)
{

// $members=member::whereNotNull('strava_id')->where->get();

// foreach($members as $member)
// {

$this->cek_token($member);


$response = Http::withToken($member->access_token)
->get('https://www.strava.com/api/v3/athlete/activities',[
'after'=>Carbon::now()->startofYear()->timestamp,
'before'=>Carbon::now()->timestamp,
'per_page'=>50,
]);

$total=array_reverse(json_decode($response,true));

// dd($total);
if(!empty($total))
{
foreach($total as $hasil)
{

// if(memberRun::find($hasil['id']))
// break;

$this->update_run($hasil,$member);

$this->hitung_resume($member,$hasil['type']);
}
echo $member->nama."<br>";
}
}

// }

}
